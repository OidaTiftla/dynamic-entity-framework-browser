﻿using DynamicEntityFrameworkBrowser.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;

namespace DynamicEntityFrameworkBrowser.Controllers {

    public class HomeController : Controller {
        private readonly ILogger<HomeController> logger_;
        private static IServiceScope scope_;
        private static Data.Lib.Data.DummyDBContext context_;
        private static Data.Lib.DbContextBrowser browser_;

        public HomeController(
            ILogger<HomeController> logger,
            IServiceProvider provider) {
            this.logger_ = logger;
            if (browser_ is null) {
                scope_ = provider.CreateScope();
                context_ = scope_.ServiceProvider.GetService<Data.Lib.Data.DummyDBContext>();
                browser_ = new Data.Lib.DbContextBrowser(context_);
            }
        }

        public IActionResult Index() {
            browser_.CurrentObject = null;
            return RedirectToAction(nameof(this.Browse));
        }

        public IActionResult Browse(string navProp = null, string navList = null, int? navIndex = null) {
            ViewBag.Result = browser_.Browse(navProp: navProp, navList: navList, navIndex: navIndex);
            return View();
        }

        public IActionResult Privacy() {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error() {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
