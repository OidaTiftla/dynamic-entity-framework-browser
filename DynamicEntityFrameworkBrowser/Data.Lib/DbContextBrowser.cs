﻿using ExtensionMethods;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Data.Lib {

    public class DbContextBrowser {
        private readonly DbContext context_;
        public object CurrentObject { get; set; }

        public DbContextBrowser(DbContext context) {
            this.context_ = context;
        }

        private IEnumerable<(string name, Func<IEnumerable<object>> get)> getTables_() {
            return getNavigationLists_(this.context_);
        }

        private IDictionary<string, object> getProperties_(object obj = null) {
            obj ??= this.CurrentObject;
            return obj.GetType().GetProperties()
                .Where(x => x.PropertyType.IsSimpleType())
                .ToDictionary(x => x.Name, x => x.GetValue(obj));
        }

        private IEnumerable<(string name, Func<object> get)> getNavigationProperties_(object obj = null) {
            obj ??= this.CurrentObject;
            return obj.GetType().GetProperties()
                .Where(x => !x.PropertyType.IsSimpleType())
                .Where(x => !typeof(IEnumerable<object>).IsAssignableFrom(x.PropertyType))
                .Select(x => (
                    x.Name,
                    (Func<object>)(() => {
                        if (obj != this.context_) {
                            this.context_.Entry(obj)
                                .Reference(x.Name)
                                .Load();
                        }
                        return x.GetValue(obj);
                    })));
        }

        private IEnumerable<(string name, Func<IEnumerable<object>> get)> getNavigationLists_(object obj = null) {
            obj ??= this.CurrentObject;
            return obj.GetType().GetProperties()
                .Where(x => !x.PropertyType.IsSimpleType())
                .Where(x => typeof(IEnumerable<object>).IsAssignableFrom(x.PropertyType))
                .Select(x => (
                    x.Name,
                    (Func<IEnumerable<object>>)(() => {
                        if (obj != this.context_) {
                            this.context_.Entry(obj)
                                .Collection(x.Name)
                                .Load();
                        }
                        return (IEnumerable<object>)x.GetValue(obj);
                    })));
        }

        public BrowsingResult Browse(string navProp = null, string navList = null, int? navIndex = null) {
            if (this.CurrentObject is null) {
                if (navProp != null) {
                    throw new ArgumentException($"Cannot navigate to {nameof(navProp)} because current object is null.", nameof(navProp));
                }
                if (navIndex != null) {
                    throw new ArgumentException($"Cannot navigate to {nameof(navIndex)} because current object is null.", nameof(navIndex));
                }
                if (navList != null) {
                    var func = this.getTables_().Where(x => x.name == navList).Select(x => x.get).FirstOrDefault();
                    if (func is null) {
                        throw new ArgumentException($"'{navList}' not found.", nameof(navList));
                    }
                    this.CurrentObject = func().Take(1000).ToList();
                }
            } else if (this.CurrentObject is IEnumerable<object> list) {
                if (navProp != null) {
                    throw new ArgumentException($"Cannot navigate to {nameof(navProp)} because current object is a list.", nameof(navProp));
                }
                if (navList != null) {
                    throw new ArgumentException($"Cannot navigate to {nameof(navList)} because current object is a list.", nameof(navList));
                }
                if (navIndex != null) {
                    if (navIndex.Value < 0
                        || navIndex.Value >= list.Count()) {
                        throw new ArgumentOutOfRangeException(nameof(navIndex));
                    }
                    this.CurrentObject = list.ElementAt(navIndex.Value);
                }
            } else {
                if (navIndex != null) {
                    throw new ArgumentException($"Cannot navigate to {nameof(navIndex)} because current object is an object.", nameof(navIndex));
                }
                if (navProp != null
                    && navList != null) {
                    throw new ArgumentException($"Cannot navigate in both directions ({nameof(navProp)} and {nameof(navList)}).", nameof(navList));
                }
                if (navProp != null) {
                    var func = this.getNavigationProperties_().Where(x => x.name == navProp).Select(x => x.get).FirstOrDefault();
                    if (func is null) {
                        throw new ArgumentException($"'{navProp}' not found.", nameof(navProp));
                    }
                    this.CurrentObject = func();
                }
                if (navList != null) {
                    var func = this.getNavigationLists_().Where(x => x.name == navList).Select(x => x.get).FirstOrDefault();
                    if (func is null) {
                        throw new ArgumentException($"'{navList}' not found.", nameof(navList));
                    }
                    this.CurrentObject = func().Take(1000).ToList();
                }
            }

            if (this.CurrentObject is null) {
                return new BrowsingResult() {
                    NavigationLists = this.getTables_().Select(x => x.name).ToList(),
                };
            } else if (this.CurrentObject is IEnumerable<object> list) {
                return new BrowsingResult() {
                    Items = list.Select(x => this.getProperties_(x)).ToList(),
                };
            } else {
                return new BrowsingResult() {
                    Properties = this.getProperties_(),
                    NavigationProperties = this.getNavigationProperties_().Select(x => x.name).ToList(),
                    NavigationLists = this.getNavigationLists_().Select(x => x.name).ToList(),
                };
            }
        }
    }

    public class BrowsingResult {
        public IDictionary<string, object> Properties { get; set; } = new Dictionary<string, object>();
        public IEnumerable<string> NavigationProperties { get; set; } = new List<string>();
        public IEnumerable<string> NavigationLists { get; set; } = new List<string>();
        public IEnumerable<IDictionary<string, object>> Items { get; set; } = new List<IDictionary<string, object>>();
    }
}
