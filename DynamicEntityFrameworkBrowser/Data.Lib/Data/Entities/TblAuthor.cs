using System.Collections.Generic;

namespace Data.Lib.Data.Entities {

    public partial class TblAuthor {

        public TblAuthor() {

            #region Generated Constructor

            AuhthorTblBooks = new HashSet<TblBook>();

            #endregion Generated Constructor
        }

        #region Generated Properties

        public int Id { get; set; }

        public string AuthorName { get; set; }

        public string Country { get; set; }

        #endregion Generated Properties

        #region Generated Relationships

        public virtual ICollection<TblBook> AuhthorTblBooks { get; set; }

        #endregion Generated Relationships
    }
}
