namespace Data.Lib.Data.Entities {

    public partial class TblBook {

        public TblBook() {
        }

        #region Generated Properties

        public int Id { get; set; }

        public int? AuhthorId { get; set; }

        public int? Price { get; set; }

        public int? Edition { get; set; }

        #endregion Generated Properties

        #region Generated Relationships

        public virtual TblAuthor AuhthorTblAuthor { get; set; }

        #endregion Generated Relationships
    }
}
