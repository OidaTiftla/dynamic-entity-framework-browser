using Microsoft.EntityFrameworkCore;

namespace Data.Lib.Data.Mapping {

    public partial class TblBookMap
        : IEntityTypeConfiguration<Entities.TblBook> {

        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Entities.TblBook> builder) {

            #region Generated Configure

            // table
            builder.ToTable("tblBook", "dbo");

            // key
            builder.HasKey(t => t.Id);

            // properties
            builder.Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

            builder.Property(t => t.AuhthorId)
                .HasColumnName("Auhthor_id")
                .HasColumnType("int");

            builder.Property(t => t.Price)
                .HasColumnName("Price")
                .HasColumnType("int");

            builder.Property(t => t.Edition)
                .HasColumnName("Edition")
                .HasColumnType("int");

            // relationships
            builder.HasOne(t => t.AuhthorTblAuthor)
                .WithMany(t => t.AuhthorTblBooks)
                .HasForeignKey(d => d.AuhthorId)
                .HasConstraintName("FK__tblBooks__Auhtho__267ABA7A");

            #endregion Generated Configure
        }

        #region Generated Constants

        public struct Table {
            public const string Schema = "dbo";
            public const string Name = "tblBook";
        }

        public struct Columns {
            public const string Id = "Id";
            public const string AuhthorId = "Auhthor_id";
            public const string Price = "Price";
            public const string Edition = "Edition";
        }

        #endregion Generated Constants
    }
}
