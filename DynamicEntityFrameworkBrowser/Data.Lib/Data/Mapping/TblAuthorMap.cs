using Microsoft.EntityFrameworkCore;

namespace Data.Lib.Data.Mapping {

    public partial class TblAuthorMap
        : IEntityTypeConfiguration<Entities.TblAuthor> {

        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Entities.TblAuthor> builder) {

            #region Generated Configure

            // table
            builder.ToTable("tblAuthor", "dbo");

            // key
            builder.HasKey(t => t.Id);

            // properties
            builder.Property(t => t.Id)
                .IsRequired()
                .HasColumnName("Id")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

            builder.Property(t => t.AuthorName)
                .HasColumnName("Author_name")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.Country)
                .HasColumnName("country")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            // relationships

            #endregion Generated Configure
        }

        #region Generated Constants

        public struct Table {
            public const string Schema = "dbo";
            public const string Name = "tblAuthor";
        }

        public struct Columns {
            public const string Id = "Id";
            public const string AuthorName = "Author_name";
            public const string Country = "country";
        }

        #endregion Generated Constants
    }
}
