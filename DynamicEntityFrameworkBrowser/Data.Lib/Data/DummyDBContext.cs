using Microsoft.EntityFrameworkCore;

namespace Data.Lib.Data {

    public partial class DummyDBContext : DbContext {

        public DummyDBContext(DbContextOptions<DummyDBContext> options)
            : base(options) {
        }

        #region Generated Properties

        public virtual DbSet<Entities.TblAuthor> TblAuthors { get; set; }

        public virtual DbSet<Entities.TblBook> TblBooks { get; set; }

        #endregion Generated Properties

        protected override void OnModelCreating(ModelBuilder modelBuilder) {

            #region Generated Configuration

            modelBuilder.ApplyConfiguration(new Mapping.TblAuthorMap());
            modelBuilder.ApplyConfiguration(new Mapping.TblBookMap());

            #endregion Generated Configuration
        }
    }
}
