CREATE Table tblAuthor
(
    Id int identity primary key,
    Author_name nvarchar(50),
    country nvarchar(50)
)

CREATE Table tblBook
(
    Id int identity primary key,
    Auhthor_id int foreign key references tblAuthor(Id),
    Price int,
    Edition int
)
