# Dynamic Entity Framework Browser

## Create database

Create database using the scripts under [./DummyData](./DummyData).

And ensure all tables have primary keys. If not you can create them with:

```sql
ALTER TABLE [tableName]
ADD [id] BIGINT PRIMARY KEY IDENTITY(1,1) NOT NULL;
```

## Create/Update entity framework core code first models

- Install the tool: `dotnet tool install --global EntityFrameworkCore.Generator`
- Navigate to the location of the target files: `cd .\DynamicEntityFrameworkBrowser\Data.Lib\`
- Remove current folder: `rm .\Data\`
- Create/Update code first models: `efg generate -c <ConnectionString>`
  - How to create a connection string: [Connection String Syntax](https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/connection-string-syntax)
